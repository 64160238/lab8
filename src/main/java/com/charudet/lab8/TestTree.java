package com.charudet.lab8;

public class TestTree {
	private String name;
	private char symbol;
	private int x;
	private int y;
	public static final int MIN_X = 0;
	public static final int MIN_Y = 0;
	public static final int MAX_X = 19;
	public static final int MAX_Y = 19;

	public TestTree(String string, char c, int i, int j) {
	}

	public void Tree(String name, char symbol, int x, int y) {
		this.name = name;
		this.symbol = symbol;
		this.x = x;
		this.y = y;
	}

	public void print() {
		System.out.println("Tree: " + name + " X:" + x + " Y:" + y);
	}

	public void setName(String name) { // Setter methods
		this.name = name;
	}

	public String getName() { // getter Methods
		return name;
	}

	public char getSymbol() {
		return symbol;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}
