package com.charudet.lab8;

public class SquareShape {
	private String name;
	private int width;
	private int height;

	public SquareShape(String name, int width, int height) {
		this.name = name;
		this.width = width;
		this.height = height;
	}

	public void printRectArea() {
		System.out.println(name + " = " + (width*height));
	}

	public void printPerimeter() {
		System.out.println(name + " = " + (2 * width) + (2 * height));
	}
}
