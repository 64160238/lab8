package com.charudet.lab8;

public class CircleTestShape {
	public static void main(String[] args) {
		CircleShape rect1 = new CircleShape("Circle1", 1);
		CircleShape rect2 = new CircleShape("Circle1", 2);
		CircleShape rect11 = new CircleShape("Circle11", 1);
		CircleShape rect22 = new CircleShape("Circle22", 2);
		rect1.printR1Area();
		rect11.printR2Area();
		rect2.printR1Area();
		rect22.printR2Area();
	}
}
