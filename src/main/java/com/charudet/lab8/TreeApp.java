package com.charudet.lab8;

public class TreeApp {
	public static void main(String[] args) {
		TestTree tree1 = new TestTree("tree1", 'T', 5, 10);
		tree1.print();
		
		TestTree tree2 = new TestTree("Tree2", 't', 5, 11);
		tree2.print();

		for (int y = TestTree.MIN_Y; y <= TestTree.MAX_Y; y++) {
			for (int x = TestTree.MIN_X; x <= TestTree.MAX_X; x++) {
				if (tree1.getX() == x && tree1.getY() == y) {
					System.out.print(tree1.getSymbol());
				} else if (tree2.getX() == x && tree2.getY() == y) {
					System.out.print(tree2.getSymbol());
				} else {
					System.out.print("-");
				}
			}
			System.out.println();
		}
	}
}
