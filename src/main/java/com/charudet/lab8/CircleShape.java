package com.charudet.lab8;

public class CircleShape {
	private String name;
	private int radius;

	public CircleShape(String name, int radius) {
		this.name = name;
		this.radius = radius;
	}

	public void printR1Area() {
		System.out.println(name + '-' + (Math.PI * Math.pow(radius, radius)));
	}

	public void printR2Area() {
		System.out.println(name + '-' + (2 * Math.PI * Math.pow(radius, radius)));
	}
}
