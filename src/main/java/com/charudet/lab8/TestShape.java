package com.charudet.lab8;

public class TestShape {

	public static void main(String[] args) {
		SquareShape rect1 = new SquareShape("rect1", 10, 5);
		SquareShape rect2 = new SquareShape("rect1", 3, 5);
		rect1.printRectArea();
		rect1.printPerimeter();
		rect2.printRectArea();
		rect2.printPerimeter();
	}

}
